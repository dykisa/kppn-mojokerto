package com.kppnmojokerto.infopintar2018.dummy;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample name for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class AboutContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<ItemPreference> ITEMS = new ArrayList<ItemPreference>();

    private static final int COUNT = 25;

    static {
        addItem(createPreferenceItem("App Name", "Info Pintar 2018"));
        addItem(createPreferenceItem("Version", "1.0"));
        addItem(createPreferenceItem("Author", "KPPN Mojokerto"));
    }

    public static void addItem(ItemPreference item) {
        ITEMS.add(item);
    }

    public static ItemPreference createPreferenceItem(String name, String value) {
        return new ItemPreference(name, value);
    }

    /**
     * A preference item representing a piece of name.
     */
    public static class ItemPreference {
        public final String name;
        public final String value;

        public ItemPreference(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}

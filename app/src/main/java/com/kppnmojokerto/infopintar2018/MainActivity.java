package com.kppnmojokerto.infopintar2018;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.kppnmojokerto.infopintar2018.dummy.AboutContent;

public class MainActivity extends AppCompatActivity implements AboutFragment.OnListFragmentInteractionListener {

    private FragmentManager fm;
    private Fragment pdfViewFragment;
    private Fragment aboutFragment;
    private Fragment active;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fm.beginTransaction().hide(active).show(pdfViewFragment).commit();
                    active = pdfViewFragment;
                    return true;
                case R.id.navigation_dashboard:
                    fm.beginTransaction().hide(active).show(aboutFragment).commit();
                    active = aboutFragment;
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupFragment();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void setupFragment() {
        fm = getSupportFragmentManager();
        pdfViewFragment = new PDFViewFragment();
        aboutFragment = AboutFragment.newInstance(1);
        active = pdfViewFragment;

        fm.beginTransaction().add(R.id.main_container, aboutFragment, "aboutFragment").hide(aboutFragment).commit();
        fm.beginTransaction().add(R.id.main_container, pdfViewFragment, "pdfViewFragment").commit();
    }

    @Override
    public void onListFragmentInteraction(AboutContent.ItemPreference item) {

    }
}
